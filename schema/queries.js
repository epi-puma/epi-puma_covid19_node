const { gql } = require('apollo-server');


const schema = gql`
  extend type Query {

    echo_covid19(message: String): String,
    all_covid19_covariables(limit: Int!, filter: String!): [CovariableCOVID19!]!
    ocurrence_covid19_by_id_covariable(id_covariable: Int!, filter: String!): [OcurrenceCOVID19!]!
  
  }
`;

module.exports = schema;