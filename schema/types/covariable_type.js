const { gql } = require('apollo-server');

module.exports = gql`
  type CovariableCOVID19 implements Covariable @key(fields: "id") {
	id: ID!,
  	resultado_lab: String,
	cells_state: [String!]!,
	cells_mun: [String!]!,
	cells_ageb: [String!]!
  }
`;
